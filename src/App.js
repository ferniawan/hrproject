import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from './routes';

function App() {
  return (
    <div className="App">
      <header>
        <div>ini header</div>
      </header>
      <div class = "containers">
        <Routes />
      </div>
      <footer>ini footer</footer>

    </div>
  );
}

export default App;
