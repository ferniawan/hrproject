
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './containers/login';
import Dashboard from './containers/dashboard';

const Routes = () => (
    <Switch>
      <Route exact path= "/" component={Login}/>
      <Route exact path= "/dashboard" component={Dashboard}/>
    </Switch>
);

export default Routes;
