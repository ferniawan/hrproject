import React, {Component} from 'react';
import {connect} from 'react-redux';
import { pushUsername, pushLoginStatus} from '../../actions/login';

class LoginInformation extends Component {

  constructor(props){
    super(props);
    this.state = {
      username: this.props.username
    }
  }

  componentDidMount(){
    this.setState({username: "badu"});
  }

  shouldComponentUpdate(nextProps, nextState){
    if(nextProps.username == ""){
      return false;
    }else{
      return true;
    }
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.username !== prevState.username){
      return ({username: nextProps.username});
    }
    else return null;
  }

  componentDidUpdate(prevProps, prevState){
    console.log(this.state.username);
  }

  componentWillUnmount(){
    console.log("unmount");
  }

  handleLoginStatus = (e) => {
    this.props.pushLoginStatus(e.target.checked)
  }

  render(){
    return (
      <div>
        Informasi seputar login <br/>
        Maaf nama username {this.state.username} tidak ditemukan
        <br/>
        <input type = "checkbox" onClick = {this.handleLoginStatus}/> ubah nilai dari statuslogin redux
      </div>
    )
  }
}

function mapStateToProps(state){
    return {
      username: state.username,
      loginStatus: state.loginStatus
    }
}

export default connect(
  mapStateToProps, {pushUsername, pushLoginStatus}
)(LoginInformation);
