import axios from "axios";
import Config from "../../config";
const APIKEY = Config.apikey;
const ROOT_URL = Config.rooturl;

export function cekAuth(){
  return {
    headers: {
      Authorization: APIKEY
    }
  }
}

export function pushLogin(email, password){
  const url = `${ROOT_URL}api/login`;
  const fdata = new FormData();
  fdata.append("username", email);
  fdata.append("password", password);
  const request = axios.post(url, fdata, {
      onUploadProggress: progressEvent => {
        console.log(progressEvent.loaded / progressEvent.total);
      }
  })
  return request;
}

export function fetchUser(){
  const url = `${ROOT_URL}api/users?page=2`;
  const request = axios.get(url);
  return request;
}

export function createUsers(email, password){
  const url = `${ROOT_URL}api/users`;
  const fdata = new FormData();
  fdata.append("name", email);
  fdata.append("job", password);
  const request = axios.post(url, fdata, {
      onUploadProggress: progressEvent => {
        console.log(progressEvent.loaded / progressEvent.total);
      }
  })
  return request;
}

export function deleteUser(userId){
  const url = `${ROOT_URL}api/users/${userId}`;
  const request = axios.delete(url);
  return request;
}

export function putUsers(email, password){
  const url = `${ROOT_URL}api/users/2`;
  const fdata = new FormData();
  fdata.append("name", email);
  fdata.append("job", password);
  const request = axios.put(url, fdata, {
      onUploadProggress: progressEvent => {
        console.log(progressEvent.loaded / progressEvent.total);
      }
  })
  return request;
}
