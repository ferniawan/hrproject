import React , { Component } from 'react';
import LoginInformation from '../components/login/logininformation';
import { pushLogin, fetchUser, createUsers, deleteUser, putUsers } from '../api/login';

class Login extends Component {

  constructor(props){
    super(props);
    this.state = {
      username: "",
      password: ""
    }
  }

  handleUsername = (val) => {
    this.setState({ username: val.target.value });
  }

  handlePassword = (val) => {
    this.setState({ password: val.target.value });
  }

  handleSubmit = () => {
    console.log(this.state.username, this.state.password);

    //axios post login
    pushLogin(this.state.username, this.state.password).then( response => {
      console.log(response);
    }).catch(error => {
      console.log("tidak berhasil");
    })

    //axios get users
    fetchUser().then( response => {
      console.log(response);
    }).catch(error => {
      console.log("tidak berhasil");
    })

    //axios post create user
    createUsers(this.state.username, this.state.password).then( response => {
      console.log(response);
    }).catch(error => {
      console.log("tidak berhasil");
    })

    //axios delete users
    deleteUser(2).then( response => {
      console.log(response);
    }).catch(error => {
      console.log("tidak berhasil");
    })

    //axios put update user
    putUsers(this.state.username, this.state.password).then( response => {
      console.log(response);
    }).catch(error => {
      console.log("tidak berhasil");
    })




  }

  render(){
    return (
      <div>
        <LoginInformation username = {this.state.username}/>
        <input type = "text" value ={this.state.username} onChange = {this.handleUsername} />
        <input type = "password" value ={this.state.password} onChange = {this.handlePassword} />
        <button onClick = {this.handleSubmit} > submit</button>
      </div>
    )
  }
}

export default Login;
