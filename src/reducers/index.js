import {combineReducers} from 'redux';

import GetUsername from './login/get_user_name';
import GetLoginStatus from './login/get_login_status';

const rootReducer = combineReducers({
  username: GetUsername,
  loginStatus: GetLoginStatus
});

export default rootReducer;
