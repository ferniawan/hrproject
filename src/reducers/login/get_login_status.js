import { LOGINSTATUS } from '../../actions/login';

export default function (state=false, action){
  switch(action.type){
    case LOGINSTATUS:
    return action.payload
  }
  return state;
}
