export const USERNAME = "USERNAME";
export const LOGINSTATUS = "LOGINSTATUS";

export function pushUsername(values){
  return {
    type: USERNAME,
    payload: values.data
  }
}

export function pushLoginStatus(values){
  return {
    type: LOGINSTATUS,
    payload: values
  }
}
